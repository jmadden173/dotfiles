# Keybindings

`<leader> f`    Open file
`<leader> g`    Fugitive
`<leader> n`    NERDTree
