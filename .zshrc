# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' matcher-list '' '' '' ''
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' verbose true
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit

# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
# shared history file
setopt sharehistory appendhistory
# add commands as it is typed
setopt incappendhistory
# expire duplicates first
setopt hist_expire_dups_first
# do not store duplications
setopt hist_ignore_dups
# ignore dups when searching
setopt hist_find_no_dups
# remove blank lines from history
setopt hist_reduce_blanks

setopt autocd beep
bindkey -v
# End of lines configured by zsh-newuser-install

# fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# version control
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst

# format vcs_info_msg_0_ var
zstyle ':vcs_info:git:*' formats "%F{yellow}(%b)%f "

# setup prompt
PROMPT=$'%B%F{green}%n@%M%f%b:%~ ${vcs_info_msg_0_}%B\n%F{magenta}>%f%b '
RPROMPT='[%(?.%?.%F{red}%?%f)]'

# colored ls commands
alias ls='ls --color=auto -X'
alias la='ls -a'
alias ll='ls -lh'
alias lla='ls -lha'

# ensure I dont do an opsie
alias rm='rm -I'

# requires copying of terminfo files
alias ssh='kitty +kitten ssh'

# 
setopt +o nomatch

# dotfiles
alias dotfiles="/usr/bin/env git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
compdef dotfiles="git"

# dont allow pip to install package 
alias pip="pip --require-virtualenv"

# Add ~/.bin to path
export PATH="$HOME/.bin:$PATH"

# STM tools
alias STM32CubeMX="/opt/STMicroelectronics/STM32Cube/STM32CubeMX/STM32CubeMX"
alias STM32CubeProgrammer="/opt/STMicroelectronics/STM32Cube/STM32CubeProgrammer/bin/STM32CubeProgrammerLauncher"
alias STM32CubeMonitor="/opt/STMicroelectronics/STM32CubeMonitor-Power/jre/java -jar /opt/STMicroelectronics/STM32CubeMonitor-Power/STM32CubeMonitor-Power.jar"

#compdef pio
_pio() {
  eval $(env COMMANDLINE="${words[1,$CURRENT]}" _PIO_COMPLETE=complete-zsh  pio)
}
if [[ "$(basename -- ${(%):-%x})" != "_pio" ]]; then
  compdef _pio pio
fi

alias utvpn="sudo echo \"Sudo first\" && openconnect https://vpn.ucsc.edu --useragent=AnyConnect --cookieonly | sudo openconnect https://vpn.ucsc.edu --useragent=AnyConnect --cookie-on-stdin"
