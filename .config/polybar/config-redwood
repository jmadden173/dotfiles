;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;==========================================================

[colors]
background = ${xrdb:background}
;background = #222
background-alt = #444
foreground = ${xrdb:foreground}
;foreground = #dfdfdf
foreground-alt = #555
primary = #4b5f9a
secondary = #e60053
alert = #bd2c40

[bar/mybar]
monitor = ${env:MONITOR:DP-0}
width = 1920
height = 32
radius = 0.0
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 0
border-color = ${colors.foreground-alt}

padding-left = 0
padding-right = 0

module-margin-left = 2
module-margin-right = 0

font-0 = "Fira Mono:style=Regular:size=10;2"
font-1 = "Font Awesome 5 Free:size=10;2"
font-2 = "Font Awesome 5 Free Solid:size=10;2"

modules-left = bspwm window
modules-center = spotify pulseaudio
modules-right = cpu temperature memory network vpn date powermenu

tray-position = left
tray-padding = 2
;tray-transparent = true
;tray-background = #0063ff

wm-restack = bspwm

;override-redirect = true

scroll-up = bspwm-desknext
scroll-down = bspwm-deskprev

cursor-click = pointer
cursor-scroll = ns-resize


[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 1

label-occupied = %index%
label-occupied-padding = 1

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 1

label-empty = 
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 1


[module/spotify]
type = custom/script
interval = 1
format = <label>
format-prefix = " "
exec = /usr/bin/python /usr/share/polybar/scripts/spotify_status.py -f '{artist}: {song}'


[module/cpu]
type = internal/cpu
interval = 1
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
format-underline = ${colors.secondary}
label = %percentage:02%%


[module/memory]
type = internal/memory
interval = 1
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
format-underline = ${colors.secondary}
label = %mb_used%


[module/network]
type = internal/network
interface = enp6s0
interval = 3.0

format-connected = <label-connected>
format-connected-underline = ${colors.secondary}
label-connected = %{T3} %{T-}%downspeed% %{T3} %{T-}%upspeed%

;format-disconnected =
format-disconnected =
;format-disconnected-underline = ${self.format-connected-underline}
label-disconnected = %{T3} %{T-}n/a
;label-disconnected-foreground = ${colors.foreground-alt}


[module/vpn]
type = internal/network
interface = tun0
interval = 3.0

format-connected = <label-connected>
format-connected-underline = ${colors.secondary}
label-connected = %{T3} %{T-}%ifname%

format-disconnected =


[module/date]
type = internal/date
interval = 5

date =
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

; Awesome Font Clock
format-prefix = 
format-prefix-foreground = ${colors.foreground}
format-underline = ${colors.secondary}

label = %date% %time%


[module/temperature]
type = internal/temperature
#thermal-zone = 0
hwmon-path = /sys/devices/platform/asus-wmi-sensors/hwmon/hwmon0/temp1_input
warn-temperature = 60
units = true

format-prefix = " "
format = <label>
format-warn-prefix = " "
format-warn = <label-warn>

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}


[module/powermenu]
type = custom/menu

expand-right = false

menu-0-0 = %{B#bd2c40} Shutdown %{B-}
menu-0-0-exec = "sudo shutdown now"

label-open = %{B#bd2c40}  %{B-}
label-close = %{B#ffb52a} Cancel %{B-}


[module/window]
type=internal/xwindow

format-prefix = " "
format = <label>
;format-underline = ${colors.primary}

label = %title%
label-maxlen = 60

label-empty = 


[module/pulseaudio]
type = internal/pulseaudio

format-volume-prefix = " "
format-volume = <label-volume>
format-muted-prefix = " "
format-muted = <label-muted>

label-volume = %percentage%%
label-muted = 0%


[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over


[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini
