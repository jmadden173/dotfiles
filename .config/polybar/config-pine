;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;==========================================================

[colors]
background = ${xrdb:color0}
;background = #222
background-alt = #444
foreground = ${xrdb:color7}
;foreground = #dfdfdf
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[bar/mybar]
;monitor = ${env:MONITOR:HDMI-1}
width = 3200
height = 38
;offset-x = 1%
;offset-y = 1%
;offset-x = 12
;offset-y = 12
radius = 0.0
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #f00

border-size = 0
border-color = ${colors.foreground-alt}


padding-left = 0
padding-right = 0

module-margin-left = 2
module-margin-right = 0

font-0 = "Source Code Pro:style=Regular:size=14;4"
font-1 = "Font Awesome 5 Free:size=14;4"
font-2 = "Font Awesome 5 Free Solid:size=14;4"

modules-left = bspwm
modules-center = spotify
modules-right = cpu temperature memory vpn wlan battery date powermenu

tray-position = left
tray-padding = 2
;tray-transparent = true
;tray-background = #0063ff

;wm-restack = bspwm
;wm-restack = i3

;override-redirect = true

;scroll-up = bspwm-desknext
;scroll-down = bspwm-deskprev

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize


[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 1

label-occupied = %index%
label-occupied-padding = 1

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 1

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 1

; Separator in between workspaces
; label-separator = |


[module/mpd]
type = internal/mpd
format-online = <icon-prev> <label-song> <icon-next>

; icon-prev = 
; icon-next =  
icon-prev = <
icon-next = >
icon-stop =  
icon-play = 
icon-pause = 

label-song-maxlen = 100
label-song-ellipsis = false


[module/spotify]
type = custom/script
interval = 1
format = <label>
format-prefix = " "
exec = /usr/bin/python $HOME/.config/polybar/polybar-spotify/spotify_status.py -f '{artist}: {song}'



[module/cpu]
type = internal/cpu
interval = 1
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
label = %percentage:02%%


[module/memory]
type = internal/memory
interval = 1
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
label = %mb_used%


[module/wlan]
type = internal/network
interface = wlp2s0
interval = 3.0

format-connected = <label-connected>
label-connected = %{T3} %{T-}%essid%

;format-disconnected =
format-disconnected =
;format-disconnected-underline = ${self.format-connected-underline}
label-disconnected = %{T3} %{T-}n/a
;label-disconnected-foreground = ${colors.foreground-alt}


[module/vpn]
type = internal/network
interface = tun0
interval = 3.0

format-connected = <label-connected>
label-connected = %{T3} %{T-}%ifname%

format-disconnected =


[module/date]
type = internal/date
interval = 5

date =
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

; Awesome Font Clock
format-prefix = 
format-prefix-foreground = ${colors.foreground}

label = %date% %time%


[module/battery]
type = internal/battery
battery = BAT0
adapter = AC0
full-at = 98

format-charging = <animation-charging> <label-charging>

format-discharging = <ramp-capacity> <label-discharging>

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-foreground = ${colors.foreground}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-foreground = ${colors.foreground}
animation-charging-framerate = 500


[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format-prefix = " "
format = <label>
format-warn-prefix = " "
format-warn = <label-warn>

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}


[module/powermenu]
type = custom/menu

expand-right = false

menu-0-0 = %{B#bd2c40} Shutdown %{B-}
menu-0-0-exec = "sudo shutdown now"

label-open = %{B#bd2c40}  %{B-}
label-close = %{B#ffb52a} Cancel %{B-}


[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over


[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
