# Wallpapers

[[_TOC_]]

## [52hz](https://corneliusdammrich.com/52hz)

![](./52hz.jpg)

## [Old Tokiyo](https://www.artstation.com/artwork/J3aRD)

![](./arseniy-chebynkin-oldtokiyo.jpg)

## [Starry Sky](https://www.pixiv.net/en/artworks/69873967)

![](./starry_sky.jpg)
