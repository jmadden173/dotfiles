# dotfiles
Theres no place like `~`

## Installed required packages

```
sudo pacman -S git openssh xorg-server awesome rofi picom zsh fzf bat source-code-pro-fonts
```

Video drivers may need to be installed/update see: https://wiki.archlinux.org/title/Xorg#Driver_installation

## Setting up repo

The following does not take into account the conflict of files.

```
git clone --bare --recursive git@gitlab.com:jmadden173/dotfiles.git "$HOME/.dotfiles"

alias dotfiles="/usr/bin/env git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"

dotfiles checkout
dotfiles pull

dotfiles submodule update --recursive --init --remote

dotfiles config --local status.showUntrackedFiles no
```

## Autocomplete

Add the following to your `.zshrc` file to enable autocompletion for the `dotfiles` command.

```
compdef dotfiles="git"
```

## Update vim modules

Open vim and type the following to pull all plugins
```
:PluginInstall
```

YCM needs to be compiled with the following
```
sudo pacman -S cmake make gcc
cd ~/.vim/bundle/YouCompleteMe
./install.py --clangd
```


## Set default shell

Replace $USER with your username

```
usermod -s /usr/bin/zsh $USER
```

## Configuration

There are separate branches for each machine that I use. My desktop is `maple` and my laptop is `spruce`. The `master` branch is the default branch that is used for general configuration. Both `maple` and `spruce` are based on [Arch Linux](https://archlinux.org/). I use [kitty](https://sw.kovidgoyal.net/kitty/) as my terminal emulator and [zsh](https://www.zsh.org/) as my shell.

### Desktop (`maple`)

My desktop uses [GNOME](https://www.gnome.org/) as the desktop environment.

### Laptop (`spruce`)

My laptop  uses [lxqt](https://lxqt-project.org/) as the desktop environment for resource constrained environments.

