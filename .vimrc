set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" themes
Plugin 'morhetz/gruvbox'
Plugin 'rakr/vim-one'
Plugin 'joshdick/onedark.vim'
Plugin 'sickill/vim-monokai'

" fzf
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'junegunn/fzf.vim'

" writing
Plugin 'reedes/vim-pencil'
Plugin 'junegunn/goyo.vim'
Plugin 'junegunn/limelight.vim'

" syntax
" NOTE tabular must come before polyglot for markdown support
Plugin 'godlygeek/tabular'
Plugin 'sheerun/vim-polyglot'
Plugin 'lervag/vimtex'
Plugin 'fladson/vim-kitty'

" linting
Plugin 'dense-analysis/ale'

" airline
"Plugin 'vim-airline/vim-airline'
"Plugin 'vim-airline/vim-airline-themes'

" git support
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'

" perservim plugins
Plugin 'preservim/nerdtree'
Plugin 'preservim/nerdcommenter'
Plugin 'preservim/tagbar'

Plugin 'ycm-core/YouCompleteMe'

" ctags
Plugin 'ludovicchabant/vim-gutentags'

Plugin 'github/copilot.vim'

call vundle#end()
filetype plugin indent on

" vundle quick ref
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal

" vim settings
"
syntax on
filetype plugin indent on

set hidden         " allow hidden buffers
set number         " line numbers
set showcmd        " show last command
set encoding=utf-8 " encoding
set tw=79
set ttyfast        " rending, explicitly set

let g:loaded_youcompleteme = 1

" colorscheme
"set termguicolors
colorscheme monokai
let g:airline_theme='base16'
"set background=dark

" whitespace
set nowrap
set formatoptions=tcqrn1
set tabstop=2
set shiftwidth=2
set expandtab

" searching
set hlsearch
set incsearch
"set ignorecase
set smartcase
set showmatch

" terminal debug
"packadd termdebug
let g:termdebug_wide=1

" tagbar
let g:tagbar_autofocus = 1
let g:tagbar_autoclose = 1

" vim-markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_math = 1
let g:vim_markdown_fenced_languages = ['math=tex']

" airline status bar
let g:airline_powerline_fonts = 1
set laststatus=2

let maplocalleader="-"

" ALE
let g:ale_linters = {"c": [], "cpp": []}

" Zathura
let g:vimtex_view_method = 'zathura'

" Limelight
let g:limelight_conceal_ctermfg = 'lightgray'
au! User GoyoEnter Limelight
au! User GoyoLeave Limelight!

" more natural movement of windows
noremap <C-J> <C-W><C-J>
noremap <C-K> <C-W><C-K>
noremap <C-L> <C-W><C-L>
noremap <C-H> <C-W><C-H>


" global bindings
" 

" fuzzy find
nnoremap <leader>f :FZF<CR>

" open git
nnoremap <leader>g :Git<CR>

" open NERDTree
nnoremap <leader>n :NERDTreeFocus<CR>

" augroups
"


function! RunCommandInKittyWindow(cmd)
    let kitty_launch_output = system('kitten @ launch --hold ' . a:cmd)
    return kitty_launch_output
endfunction

function! RunCommandInRecentWindow(cmd)
    let rc = system('kitten @ send-text --match recent:1 ' . a:cmd)
    echo rc
    return rc
endfunction 

" set cursorline on current window only
augroup CursorLine
    au!
    au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
    au WinLeave * setlocal nocursorline
augroup END

augroup filetype_python
	autocmd!
    autocmd FileType python set tabstop=4
    autocmd FileType python set shiftwidth=4

	" runs python script
	autocmd FileType python map <buffer> <leader>rr :w<CR>:exec '!python3' shellescape(@%, 1)<CR>

	" runs python script with debugging
	autocmd FileType python map <buffer> <leader>rd :w<CR>:exec '!python3 -m pdb' shellescape(@%, 1)<CR>

    " set a text width of 79 as per PEP8
    autocmd Filetype python set tw=79
augroup END

augroup filetype_c
    autocmd!

    " always have sign column
    autocmd FileType c,cpp set signcolumn=yes
augroup END

augroup filetype_markdown
    autocmd!
    autocmd Filetype markdown,mkd let g:pencil#conceallevel=0
    autocmd FileType markdown,mkd call pencil#init({'wrap': 'soft'})
    autocmd FileType markdown,mkd nmap <buffer> <f4> :Toc<CR>
    "autocmd Filetype markdown,mkd setlocal spell spelllang=en_us
augroup END

augroup filetype_tex
    autocmd!
    autocmd Filetype tex set spell
    autocmd FileType tex set nonu
    autocmd Filetype tex let g:pencil#conceallevel=0
    autocmd FileType tex call pencil#init({'wrap': 'soft'})
augroup END

augroup filetype_rst
    autocmd!

    autocmd Filetype rst set tw=79
    autocmd FileType rst set spell
    autocmd FileType tex set nonu
augroup END

" autocmd FileType text call pencil#init()
